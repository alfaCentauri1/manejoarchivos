package manejoarchivos;

import java.io.*;

public class ManejoArchivos {

    public static void crearArchivo(String nombreArchivo){
        File archivo = new File(nombreArchivo);
        try{
            PrintWriter salida = new PrintWriter(archivo);
            salida.close();
            System.out.println("Se ha creado el archivo.");
        }catch (FileNotFoundException exception){
            exception.printStackTrace(System.out);
        }
    }

    public static void borrarArchivo(String nombreArchivo){
        File archivo = new File(nombreArchivo);
        archivo.delete();
        System.out.println("Se ha borrado el archivo " + nombreArchivo);
    }

    public static void modificarArchivo(String nombreArchivo, String contenido){
        File archivo = new File(nombreArchivo);
        try{
            PrintWriter salida = new PrintWriter(archivo);
            salida.println(contenido);
            salida.close();
            System.out.println("Se ha escrito en el archivo.");
        }catch (FileNotFoundException exception){
            exception.printStackTrace(System.out);
        }
    }

    public static void anexarArchivo(String nombreArchivo, String contenido){
        File archivo = new File(nombreArchivo);
        try{
            PrintWriter salida = new PrintWriter( new FileWriter( archivo, true ) );
            salida.println(contenido);
            salida.close();
            System.out.println("Se ha anexado en el archivo.");
        }catch (FileNotFoundException exception){
            exception.printStackTrace(System.out);
        }catch (IOException exception){
            exception.printStackTrace(System.out);
        }
    }

    public static void leerArchivo(String nombreArchivo){
        var archivo = new File(nombreArchivo);
        try {
            var entrada = new BufferedReader(new FileReader(archivo));
            String lectura;
            do {
                lectura = entrada.readLine();
                if(lectura != null)
                    System.out.println("lectura = " + lectura);
                else
                    System.out.println("EOF");
            }while(lectura != null);
            entrada.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.out);
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }
    }
}
