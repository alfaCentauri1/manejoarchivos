package manejoarchivos;

public class Ejecutable2 {
    public static void main(String args[]){
        System.out.println("Ejemplo de manejo de archivos en Java.");
        var nombreArchivo = "prueba.txt";
        ManejoArchivos.crearArchivo(nombreArchivo);
        ManejoArchivos.modificarArchivo(nombreArchivo, "Esta es una demostración de Java con archivos de texto.");
        ManejoArchivos.anexarArchivo(nombreArchivo, "Segunda linea...");
        ManejoArchivos.anexarArchivo(nombreArchivo, "Tercera linea...");
        ManejoArchivos.leerArchivo(nombreArchivo);
        System.out.println("Fin");
    }
}
